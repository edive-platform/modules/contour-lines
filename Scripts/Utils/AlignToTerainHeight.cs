﻿using UnityEngine;

namespace Edive.Environments.TerrainContours.Utils
{
    public class AlignToTerainHeight : MonoBehaviour
    {
        [SerializeField]
        private Transform objectToAlign;
        [SerializeField]
        private Transform raycastOrigin;
        [SerializeField]
        private Transform raycastEnd;
        [SerializeField]
        private LayerMask layerMask = -1;
        [SerializeField]
        private Vector3 offsetAfterAligning = Vector3.zero;

        private void OnEnable()
        {
            if (!objectToAlign) objectToAlign = transform;
            if (!raycastOrigin) raycastOrigin = objectToAlign;
            if (!raycastEnd)
            {
                raycastEnd = new GameObject().transform;
                raycastEnd.position = raycastOrigin.position + raycastOrigin.up;
            }
        }

        public bool TryToAlign()
        {
            return TryToAlign(objectToAlign, layerMask, offsetAfterAligning);
        }

        private bool TryToAlign(Transform objectToAlign, LayerMask layerMask, Vector3 offsetAfterAligning)
        {
            if (raycastOrigin && raycastEnd)
            {
                RaycastHit hitInfo;
                if (Physics.Linecast(raycastOrigin.position, raycastEnd.position, out hitInfo, layerMask))
                {
                    //Debug.Log($"Aligning to gameobject: {hitInfo.transform.gameObject.name}.");
                    objectToAlign.position = hitInfo.point + offsetAfterAligning;
                    return true;
                }
                else
                {
                    objectToAlign.position = raycastEnd.position;
                    //Debug.Log("No collision detected.");
                }
            }
            else
            {
                Debug.LogWarning("You need assign the raycast origin and end first.");
            }
            return false;
        }

        public bool AlignFlagToTable(LayerMask tableLayerMask, Vector3 offsetAfterAligning, float maxDistance)
        {
            // aligns if the flag is under the table
            if (TryToAlign(transform, tableLayerMask, offsetAfterAligning))
            {
                return true;
            }
            else
            {
                // second raycast if the flag is above the table
                if (raycastEnd)
                {
                    RaycastHit hitInfo;
                    if (Physics.Raycast(new Ray(raycastEnd.position, Vector3.down), out hitInfo, maxDistance, tableLayerMask))
                    {
                        //Debug.Log($"Aligning to gameobject: {hitInfo.transform.gameObject.name}.");
                        transform.position = hitInfo.point + offsetAfterAligning;
                        return true;
                    }
                    else
                    {
                        objectToAlign.position = raycastEnd.position;
                        //Debug.Log("No collision detected.");
                    }
                }
                else
                {
                    Debug.LogWarning("You need assign the raycast origin and end first.");
                }
            }
            return false;
        }

        private void OnDrawGizmos()
        {
            if (raycastOrigin && raycastEnd) Gizmos.DrawLine(raycastOrigin.position, raycastEnd.position);
        }
    }
}
