﻿using UnityEngine;

namespace Edive.Environments.TerrainContours.Utils
{
    public class TerrainCollisionDetection : MonoBehaviour
    {
        [SerializeField]
        Transform pointAtransform;
        [SerializeField]
        Transform pointBtransform;
        [SerializeField]
        LayerMask layerMask = -1;

        public bool Intervisibility()
        {
            return Intervisibility(pointAtransform.position, pointBtransform.position, layerMask);
        }

        public static bool Intervisibility(Vector3 pointA, Vector3 pointB, LayerMask mask)
        {
            RaycastHit hitInfo;
            if (Physics.Linecast(pointA, pointB, out hitInfo, mask))
            {
                Debug.Log($"Hit gameobject '{hitInfo.transform.gameObject.name}' at world position '{hitInfo.point}'.");
                return false;
            }
            return true;
        }
    }
}
