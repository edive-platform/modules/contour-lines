﻿using System;
using UnityEngine;

namespace Edive.Environments.TerrainContours.Terrain
{
    [Serializable]
    public class ContourMapTile : MonoBehaviour
    {
        [SerializeField]
        private bool initOnAwake = false;

        public Material materialUnlit;

        public Material materialLit;

        public Texture blankTexture;
        public Texture ortophotoTexture;

        public string mainTexParameterNameUnlit = "_MainTex";
        public string mainTexParameterNameLit = "_MainTex";

        public string contourColorParameterNameUnlit = "_LineColor";
        public string contourColorParameterNameLit = "_LineColor";
        private bool initialized = false;

        private void Awake()
        {
            if (initOnAwake) Init();
        }

        /// <summary>
        /// Whe using with Mirror, this can be called to ensure init.
        /// Mirror does not guarantee that Awake/Start is called when running OnStartClient
        /// </summary>
        public void Init()
        {
            initOnAwake = false;
            materialUnlit = Material.Instantiate(materialUnlit);
            materialLit = Material.Instantiate(materialLit);

            GetComponent<Renderer>().material = materialUnlit;

            initialized = true;
        }

        public void SetContourColor(Color color)
        {
            if (!initialized)
            {
                Debug.LogWarning("Not yet initialized");
                return;
            }
            if (materialUnlit.HasProperty(contourColorParameterNameUnlit)) materialUnlit.SetColor(contourColorParameterNameUnlit, color);
            if (materialLit.HasProperty(contourColorParameterNameLit)) materialLit.SetColor(contourColorParameterNameLit, color);
        }

        public void SetTextureOrtophoto()
        {
            if (!initialized)
            {
                Debug.LogWarning("Not yet initialized");
                return;
            }
            materialUnlit.SetTexture(mainTexParameterNameUnlit, ortophotoTexture);
            materialLit.SetTexture(mainTexParameterNameLit, ortophotoTexture);
        }

        public void SetTextureNone()
        {
            if (!initialized)
            {
                Debug.LogWarning("Not yet initialized");
                return;
            }
            materialUnlit.SetTexture(mainTexParameterNameUnlit, null);
            materialLit.SetTexture(mainTexParameterNameLit, null);
        }

        public void SetTextureBlank()
        {
            if (!initialized)
            {
                Debug.LogWarning("Not yet initialized");
                return;
            }
            materialUnlit.SetTexture(mainTexParameterNameUnlit, blankTexture);
            materialLit.SetTexture(mainTexParameterNameLit, blankTexture);
        }

        public void SetLit(bool lit)
        {
            if (!initialized)
            {
                Debug.LogWarning("Not yet initialized");
                return;
            }
            if (lit)
            {
                GetComponent<Renderer>().material = materialLit;
            }
            else
            {
                GetComponent<Renderer>().material = materialUnlit;
            }
        }

        public float GetMaterialProperty(string parameterNameID)
        {
            // both materials should have the same value
            return materialUnlit.GetFloat(parameterNameID);
        }

        public void SetMaterialProperty(string parameterNameID, float value)
        {
            if (!initialized)
            {
                Debug.LogWarning("Not yet initialized");
                return;
            }
            materialLit.SetFloat(parameterNameID, value);
            materialUnlit.SetFloat(parameterNameID, value);
        }
    }
}
