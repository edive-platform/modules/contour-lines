﻿using System;
using UnityEngine;

namespace Edive.Environments.TerrainContours.Terrain
{
    public class MapController : MonoBehaviour
    {
        [SerializeField]
        private ContourMap[] maps;

        public bool Is3D
        {
            get
            {
                return !maps[CurrentMapIndex].is2D;
            }
        }

        public bool IsBlank
        {
            get
            {
                return maps[CurrentMapIndex].isBlank;
            }
        }

        private void Start()
        {
            if (maps.Length == 0)
            {
                Debug.LogError("No maps.");
            }
        }

        public int CurrentMapIndex { get; private set; }
        public int MapsCount
        {
            get
            {
                return maps.Length;
            }
        }

        public void Toggle3D(bool value)
        {
            var m = maps[CurrentMapIndex];
            if (value) m.SwitchTo3D();
            else m.SwitchTo2D();
        }

        public void ToggleBlank(bool value)
        {
            var m = maps[CurrentMapIndex];
            if (value) m.SwitchToBlank();
            else m.SwitchToOrthophoto();
        }

        public void ShowMap(int mapIndex)
        {
            ShowMap(mapIndex, true);
        }

        public void ShowMap(int mapIndex, bool resetPrev)
        {
            if (mapIndex < 0 || mapIndex >= maps.Length)
            {
                Debug.LogError("Could not find map. Wrong map index.");
                return;
            }
            if (resetPrev)
            {
                maps[CurrentMapIndex].ResetMap();
            }
            // hide old
            maps[CurrentMapIndex].gameObject.SetActive(false);
            // show new
            maps[mapIndex].gameObject.SetActive(true);
            // update selection index
            CurrentMapIndex = mapIndex;
        }

        public void SetEquidistance(float value)
        {
            var m = maps[CurrentMapIndex];
            m.SetEquidistance(value);
        }

        public void Crop(float value)
        {
            var m = maps[CurrentMapIndex];
            m.Crop(value);
        }

        public float GetEquidistance()
        {
            return maps[CurrentMapIndex].GetEquidistance();
        }

        public float GetCropValue()
        {
            return maps[CurrentMapIndex].GetCropValue();
        }

        public void Init()
        {
            foreach (var m in maps) m.Init();
        }
    }
}
