﻿using UnityEngine;

namespace Edive.Environments.TerrainContours.Flags
{
    public class VisibilityLine : MonoBehaviour
    {
        [SerializeField]
        private LineRenderer lineRenderer;

        private Vector3 m_lineStart;
        public Vector3 LineStart
        {
            get { return m_lineStart; }
            set
            {
                m_lineStart = value;
                lineRenderer.SetPosition(0, m_lineStart);
            }
        }

        private Vector3 m_lineEnd;
        public Vector3 LineEnd
        {
            get { return m_lineEnd; }
            set
            {
                m_lineEnd = value;
                lineRenderer.SetPosition(1, m_lineEnd);
            }
        }

        public Color StartColor
        {
            get { return lineRenderer.startColor; }
            set { lineRenderer.startColor = value; }
        }

        public Color EndColor
        {
            get { return lineRenderer.endColor; }
            set { lineRenderer.endColor = value; }
        }

        public bool Visible
        {
            get
            {
                return lineRenderer.enabled;
            }
            set
            {
                lineRenderer.enabled = value;
            }
        }

        private void Awake()
        {
            lineRenderer.positionCount = 2;
        }
    }
}
