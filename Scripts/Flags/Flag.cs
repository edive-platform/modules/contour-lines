﻿using Edive.Environments.TerrainContours.Utils;
using UnityEngine;

namespace Edive.Environments.TerrainContours.Flags
{
    public class Flag : MonoBehaviour
    {
        // TODO remove alignToTerainHeight from here, and align it on grab release instead
        [SerializeField]
        private AlignToTerainHeight alignToTerainHeight;

        /// <summary>
        /// Represents the view point of the flag for checking the visibility.
        /// </summary>
        public Transform visibilityPoint;

        public bool isObjective = false;

        public bool IsOnTerrain
        {
            get
            {
                // TODO check this on grab and release, not in here
                return alignToTerainHeight.TryToAlign();
            }
        }

        [SerializeField]
        private LayerMask tableLayerMask = -1;

        private void Awake()
        {
            if (!alignToTerainHeight) alignToTerainHeight = GetComponent<AlignToTerainHeight>();
        }

        public bool IsVisibleFrom(Flag other, LayerMask intervisibilityLayerMask)
        {
            // if the flag is not on terrain
            if (!IsOnTerrain)
            {
                return false;
            }
            // if the other flag is not on terrain
            if (!other.IsOnTerrain)
            {
                return false;
            }

            return TerrainCollisionDetection.Intervisibility(visibilityPoint.position, other.visibilityPoint.position, intervisibilityLayerMask);
        }

        public void AlignToTable()
        {
            var canAlign = alignToTerainHeight.AlignFlagToTable(tableLayerMask, Vector3.zero, .48f);
            //Debug.Log("Aligning flag to table: " + canAlign);
            if (canAlign) transform.rotation = Quaternion.Euler(new Vector3(0, transform.rotation.eulerAngles.y, 0));
        }
    }
}
