﻿using Edive.Environments.TerrainContours.Flags;
using UnityEditor;
using UnityEngine;

namespace Edive.Environments.TerrainContours
{
    [CustomEditor(typeof(Flag))]
    [CanEditMultipleObjects]
    public class FlagEditor : Editor
    {
        Flag myScript;
        private void OnEnable()
        {
            myScript = (Flag)target;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GUILayout.Button("Align to Table"))
            {
                myScript.AlignToTable();
            }
        }
    }
}
