﻿using Edive.Environments.TerrainContours.Terrain;
using UnityEditor;
using UnityEngine;

namespace Edive.Environments.TerrainContours
{
    [CustomEditor(typeof(MapController))]
    [CanEditMultipleObjects]
    public class MapControllerEditor : Editor
    {
        MapController myScript;

        bool map3D = false;
        bool blind = false;

        private void OnEnable()
        {
            myScript = (MapController)target;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GUILayout.Button("Toggle 3D"))
            {
                myScript.Toggle3D(map3D = !map3D);
            }
            if (GUILayout.Button("Toggle Blind"))
            {
                myScript.ToggleBlank(blind = !blind);
            }
            if (GUILayout.Button("Next Map"))
            {
                myScript.ShowMap((myScript.CurrentMapIndex + 1) % myScript.MapsCount);
            }
        }
    }
}
