﻿using Edive.Environments.TerrainContours.Networking;
using NUnit.Framework;

namespace Edive.Environments.TerrainContours.Tests
{
    public class TerrainNetworkingTest
    {
        [Test]
        public void MaskToBoolTest()
        {
            Assert.AreEqual(false, BetterTerrainNetworking.MaskToBool(0, 1));
            Assert.AreEqual(true, BetterTerrainNetworking.MaskToBool(2, 1));
            Assert.AreEqual(true, BetterTerrainNetworking.MaskToBool(1, 0));
            Assert.AreEqual(true, BetterTerrainNetworking.MaskToBool(4, 2));
            Assert.AreEqual(true, BetterTerrainNetworking.MaskToBool(5, 2));
            Assert.AreEqual(true, BetterTerrainNetworking.MaskToBool(5, 0));
            Assert.AreEqual(false, BetterTerrainNetworking.MaskToBool(5, 1));
        }
        [Test]
        public void BoolToMaskTest()
        {
            Assert.AreEqual(0, BetterTerrainNetworking.BoolToMask(false, 0, 0));

            Assert.AreEqual(1, BetterTerrainNetworking.BoolToMask(true, 0, 0));
            Assert.AreEqual(2, BetterTerrainNetworking.BoolToMask(true, 1, 0));
            Assert.AreEqual(4, BetterTerrainNetworking.BoolToMask(true, 2, 0));
            Assert.AreEqual(8, BetterTerrainNetworking.BoolToMask(true, 3, 0));
            Assert.AreEqual(16, BetterTerrainNetworking.BoolToMask(true, 4, 0));

            Assert.AreEqual(0, BetterTerrainNetworking.BoolToMask(false, 0, 1));
            Assert.AreEqual(0, BetterTerrainNetworking.BoolToMask(false, 1, 2));
            Assert.AreEqual(0, BetterTerrainNetworking.BoolToMask(false, 2, 4));
            Assert.AreEqual(0, BetterTerrainNetworking.BoolToMask(false, 3, 8));
            Assert.AreEqual(0, BetterTerrainNetworking.BoolToMask(false, 4, 16));

            Assert.AreEqual(16, BetterTerrainNetworking.BoolToMask(false, 1, 18));
            Assert.AreEqual(18, BetterTerrainNetworking.BoolToMask(true, 1, 16));
            Assert.AreEqual(17, BetterTerrainNetworking.BoolToMask(true, 0, 16));
            Assert.AreEqual(16, BetterTerrainNetworking.BoolToMask(false, 0, 17));
        }
    }
}
