﻿using Edive.Environments.TerrainContours.Utils;
using UnityEditor;
using UnityEngine;

namespace Edive.Environments.TerrainContours
{
    [CustomEditor(typeof(ContourShaderMaterialSetter))]
    [CanEditMultipleObjects]
    public class ContourShaderMaterialSetterEditor : Editor
    {
        ContourShaderMaterialSetter myScript;
        private void OnEnable()
        {
            myScript = (ContourShaderMaterialSetter)target;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GUILayout.Button("Try to set"))
            {
                myScript.SetMinMaxVertexPosition();
            }
        }
    }
}
