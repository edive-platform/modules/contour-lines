﻿using Edive.Environments.TerrainContours.Evaluation;
using Edive.Networking;
using Mirror;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UVRN.Logging;

namespace Edive.Environments.TerrainContours.Networking
{
    public class BetterTerrainNetworking : NetworkBehaviour
    {
        #region Net Manager
        private Edive_NetworkManager m_netManager;
        private Edive_NetworkManager netManager
        {
            get
            {
                if (!m_netManager)
                {
                    m_netManager = (Edive_NetworkManager)Edive_NetworkManager.singleton;
                }
                return m_netManager;
            }
        }
        #endregion

        [Serializable]
        public class UnityEventBool : UnityEvent<bool> { }

        [Serializable]
        public class UnityEventInt : UnityEvent<int> { }

        [SerializeField]
        private TerrainManager terrainManager;

        /// <summary>
        /// Do not modify any syncvar directly, use the hooks (handle/sync) methods on server to ensure consistent state
        /// </summary>
        [SyncVar(hook = nameof(HandleToggle3D))]
        private int sv_terrain3DBitMask = 0;

        [SyncVar(hook = nameof(HandleToggleBlank))]
        private int sv_blankBitMask = int.MaxValue; // init to ones only

        [SyncVar(hook = nameof(HandleCropping))]
        private float sv_croppingValue = 0;

        [SyncVar(hook = nameof(HandleEquidistance))]
        private float sv_equidistanceValue = 0;

        [SyncVar(hook = nameof(HandleTerrainChange))]
        private int sv_currentTerrainIndex = 0;

        [SerializeField]
        private float initialEquidistance = 3;
        [SerializeField]
        private float initialCrop = 1;

        /// <summary>
        /// To be able to tell if the start method has been called.
        /// Some of the attributes of some child classes were not correctly initialized (mapController)
        /// ...not even when using custom init method.
        /// ...so we need this to be able to wait for init.
        /// </summary>
        private bool started;

        public bool IsCurrent3D => MaskToBool(sv_terrain3DBitMask, sv_currentTerrainIndex);
        public bool IsCurrentBlank => MaskToBool(sv_blankBitMask, sv_currentTerrainIndex);

        /// <summary>
        /// Returns bit value on a given index in a given bitmask
        /// </summary>
        /// <param name="mask"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static bool MaskToBool(int mask, int index)
        {
            return (mask & (1 << index)) != 0;
        }

        /// <summary>
        /// Changes just one bit in a bit mask
        /// </summary>
        /// <param name="value">1 or 0</param>
        /// <param name="index">Index which to change</param>
        /// <param name="oldMask">Mask to change</param>
        /// <returns></returns>
        public static int BoolToMask(bool value, int index, int oldMask)
        {
            bool oldValue = MaskToBool(oldMask, index);
            // dont toggle the bit if they are the same
            if (value == oldValue)
            {
                return oldMask;
            }
            // toggle the bit
            return oldMask ^ (1 << index);
        }

        private void Start()
        {
            started = true;
        }

        #region Mirror Overrides

        public override void OnStartServer()
        {
            // Reset the maps to 2D blank
            terrainManager.InitMapController();
            HandleCropping(sv_croppingValue, initialCrop);
            HandleEquidistance(sv_equidistanceValue, initialEquidistance);
        }

        public override void OnStartClient()
        {
            // OnStartClient can be called before Start or Awake, so make sure to correctly init everything before handling the syncvars

            // The state of SyncVars is applied to game objects on clients before OnStartClient() is called
            StartCoroutine(SyncAfterStart());
        }

        private IEnumerator SyncAfterStart()
        {
            terrainManager.InitMapController();

            while (!started) yield return new WaitForEndOfFrame();

            // when started is true = initialization is complete
            HandleTerrainChange(0, sv_currentTerrainIndex);
            HandleToggle3D(0, sv_terrain3DBitMask);
            HandleToggleBlank(int.MaxValue, sv_blankBitMask);
            HandleCropping(0, this.sv_croppingValue);
            HandleEquidistance(0, this.sv_equidistanceValue);
        }
        #endregion

        #region ClientHandleCalls
        private void HandleToggle3D(int _old, int _new)
        {
            // unnecessary to call this line on clients, but it is easier to call this method on server to make sure the state is consistent
            sv_terrain3DBitMask = _new;
            bool value = MaskToBool(sv_terrain3DBitMask, sv_currentTerrainIndex);
            terrainManager.Toggle3D(value);
        }

        private void HandleToggleBlank(int _old, int _new)
        {
            sv_blankBitMask = _new;
            bool value = MaskToBool(sv_blankBitMask, sv_currentTerrainIndex);
            terrainManager.ToggleBlank(value);
        }

        private void HandleTerrainChange(int _old, int _new)
        {
            sv_currentTerrainIndex = _new;
            terrainManager.SwitchMap(sv_currentTerrainIndex);
        }

        private void HandleEquidistance(float _old, float _new)
        {
            sv_equidistanceValue = _new;
            terrainManager.SetEquidistance(sv_equidistanceValue);
        }

        private void HandleCropping(float _old, float _new)
        {
            sv_croppingValue = _new;
            terrainManager.Crop(sv_croppingValue);
        }
        #endregion

        #region Public Functions

        /// <summary>
        /// Bypass to be able to call this function from Unity Editor (triggered by Unity Events).
        /// </summary>
        public void ToggleTerrain_CallCommand(int terrain)
        {
            CmdToggleMap(terrain);
        }

        /// <summary>
        /// Bypass to be able to call this function from Unity Editor (triggered by Unity Events).
        /// </summary>
        public void SetEquidistance_CallCommand(float value)
        {
            CmdSetEquidistance(value);
        }

        /// <summary>
        /// Bypass to be able to call this function from Unity Editor (triggered by Unity Events).
        /// </summary>
        public void Crop_CallCommand(float value)
        {
            CmdCrop(value);
        }

        /// <summary>
        /// Bypass to be able to call this function from Unity Editor (triggered by Unity Events).
        /// </summary>
        public void Toggle3D_CallCommand()
        {
            CmdSet3D(!IsCurrent3D);
        }

        /// <summary>
        /// Bypass to be able to call this function from Unity Editor (triggered by Unity Events).
        /// </summary>
        public void ToggleBlind_CallCommand()
        {
            CmdSetBlind(!IsCurrentBlank);
        }

        /// <summary>
        /// Bypass to be able to call this function from Unity Editor (triggered by Unity Events).
        /// </summary>
        public void CheckFlags_CallCommand()
        {
            CmdChecksFlag();
        }
        #endregion

        #region Commands
        [Command(requiresAuthority = false)]
        public void CmdToggleMap(int index, NetworkConnectionToClient sender = null)
        {
            // reset also the syncvars on the server and clients (blank + 3D) 
            HandleToggleBlank(sv_blankBitMask, BoolToMask(true, sv_currentTerrainIndex, sv_blankBitMask));
            HandleToggle3D(sv_terrain3DBitMask, BoolToMask(false, sv_currentTerrainIndex, sv_terrain3DBitMask));
            // and slider syncvars 
            HandleCropping(sv_croppingValue, initialCrop);
            HandleEquidistance(sv_equidistanceValue, initialEquidistance);
            // change the terrain
            HandleTerrainChange(sv_currentTerrainIndex, index);

            if (sender != null) UVRN_Logger.Instance.LogEntry(sender, "Terrain switched to: " + index);
        }

        [Command(requiresAuthority = false)]
        public void CmdSetEquidistance(float value, NetworkConnectionToClient sender = null)
        {
            HandleEquidistance(sv_equidistanceValue, value);

            if (sender != null) UVRN_Logger.Instance.LogEntry(sender, "Set equidistance to: " + value);
        }

        [Command(requiresAuthority = false)]
        public void CmdCrop(float value, NetworkConnectionToClient sender = null)
        {
            // update clients via syncvar hook
            HandleCropping(sv_croppingValue, value);

            // log on server
            if (sender != null) UVRN_Logger.Instance.LogEntry(sender, "Cropped terrain to level: " + value);
        }

        [Command(requiresAuthority = false)]
        public void CmdSet3D(bool value, NetworkConnectionToClient sender = null)
        {
            HandleToggle3D(sv_terrain3DBitMask, BoolToMask(value, sv_currentTerrainIndex, sv_terrain3DBitMask));

            if (sender != null) UVRN_Logger.Instance.LogEntry(sender, "Set 3D terrain visualization to: " + value);
        }

        [Command(requiresAuthority = false)]
        public void CmdSetBlind(bool value, NetworkConnectionToClient sender = null)
        {
            HandleToggleBlank(sv_blankBitMask, BoolToMask(value, sv_currentTerrainIndex, sv_blankBitMask));

            if (sender != null) UVRN_Logger.Instance.LogEntry(sender, "Set blind terrain visualization to: " + value);
        }

        [Command(requiresAuthority = false)]
        public void CmdChecksFlag(NetworkConnectionToClient sender = null)
        {
            bool visible = terrainManager.Evaluate();
            if (sender != null) UVRN_Logger.Instance.LogEntry(sender, $"Solution visible: {visible}");

            RpcCheckFlagsOnClient();
        }
        #endregion

        #region Client RPCs
        [ClientRpc]
        private void RpcCheckFlagsOnClient()
        {
            bool visible = terrainManager.Evaluate();
            Debug.Log($"Solution visible: {visible}");
        }

        #endregion
    }
}
