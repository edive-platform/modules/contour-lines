﻿using Edive.Environments.TerrainContours.Flags;
using Edive.Environments.TerrainContours.Terrain;
using Edive.Interactions.Transformations;
using Edive.UI;
using Edive.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Edive.Environments.TerrainContours.Evaluation
{
    /// <summary>
    /// TODO make this the main map controller and separate this from the networking
    /// </summary>
    public class TerrainManager : MonoBehaviour
    {
        #region UI Fields

        // VR 3D sliders
        [SerializeField]
        private MUVRE.FloatSlider equidistanceSlider;

        [SerializeField]
        private MUVRE.FloatSlider clampSlider;

        // Desktop 2D overlay sliders
        [SerializeField]
        private Slider equidistanceDesktopSlider;

        [SerializeField]
        private Slider clampDesktopSlider;

        [SerializeField]
        private SimpleToggleGroup terrainToggleGroup;

        [SerializeField]
        private Toggle[] desktopToggles;

        [SerializeField]
        private ToggleActive toggleBlankButton;

        [SerializeField]
        private Toggle toggleBlankDesktopButton;

        [SerializeField]
        private ToggleActive toggle3DButton;

        [SerializeField]
        private Toggle toggle3DDesktopButton;

        [SerializeField]
        private SlideshowController instructions;

        #endregion

        public FlagManager flagManager;

        public MapController mapController;

        public MatchTransform[] objectivesInitPositions;

        public VisibilityLines visibilityLines;

        private bool initialized;

        private void Awake()
        {
            InitMapController();
        }

        public void InitMapController()
        {
            if (!initialized)
            {
                mapController.Init();
                initialized = true;
            }
        }

        // to be able to call it from unity editor events
        public void CallEvaluate()
        {
            Evaluate();
        }

        public bool Evaluate()
        {
            return flagManager.CheckLineVisibilityThroughAllFlags();
        }

        public void ResetFlags()
        {
            flagManager.ResetFlags();

            foreach (var m in objectivesInitPositions)
            {
                if (m.gameObject.activeInHierarchy)
                {
                    m.Match();
                }
            }
        }

        public void UpdateDesktopSliders()
        {
            equidistanceDesktopSlider?.SetValueWithoutNotify(mapController.GetEquidistance());
            clampDesktopSlider?.SetValueWithoutNotify(mapController.GetEquidistance());
        }

        public void SetEquidistanceSlider(float value)
        {
            equidistanceSlider?.SetValue(value);
            equidistanceDesktopSlider?.SetValueWithoutNotify(value);
        }

        private void SetClampSlider(float value)
        {
            clampSlider?.SetValue(value);
            clampDesktopSlider?.SetValueWithoutNotify(value);
        }

        public void SwitchMap(int mapIndex)
        {
            // switch to map (should switch the old to 2D and blank)
            mapController.ShowMap(mapIndex);

            // reset Flags
            ResetFlags();

            // disable all visible lines from previous evaluation
            visibilityLines.DisableAllLines();

            // update the UI
            terrainToggleGroup.Select(mapIndex);
            if (desktopToggles != null && desktopToggles.Length == mapController.MapsCount)
            {
                desktopToggles[mapIndex].SetIsOnWithoutNotify(true);
            }

            // update instructions slide
            instructions.SetSlide(mapIndex);

            // set 2D and blank (to update the UI)
            Toggle3D(false);
            ToggleBlank(true);
        }

        private void EnableSliders(bool active)
        {
            //TODO use some interfaces or inheritance to make this nicer
            // VR
            equidistanceSlider?.gameObject.SetActive(active);
            clampSlider?.gameObject.SetActive(active);
            // Offline desktop
            if (clampDesktopSlider) clampDesktopSlider.interactable = active;
            if (equidistanceDesktopSlider) equidistanceDesktopSlider.interactable = active;

            // correct the initial value
            equidistanceSlider?.SetValue(mapController.GetEquidistance());
            clampSlider?.SetValue(mapController.GetCropValue());
        }

        public void Toggle3D(bool active)
        {
            mapController.Toggle3D(active);
            // UI
            EnableSliders(active);
            toggle3DButton?.Toggle(active);
            toggle3DDesktopButton?.SetIsOnWithoutNotify(active);
        }

        public void ToggleBlank(bool active)
        {
            mapController.ToggleBlank(active);
            // UI
            toggleBlankButton?.Toggle(active);
            toggleBlankDesktopButton?.SetIsOnWithoutNotify(active);
        }

        public void SetEquidistance(float equidistanceValue)
        {
            mapController.SetEquidistance(equidistanceValue);
            // UI
            SetEquidistanceSlider(equidistanceValue);
        }

        public void Crop(float croppingValue)
        {
            mapController.Crop(croppingValue);
            // UI
            SetClampSlider(croppingValue);
        }
    }
}
