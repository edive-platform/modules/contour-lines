# Contour Lines Environment

You can just clone this into the non-versioned `Environments` folder in the eDIVE core repository (or any other non-versioned folder). This can be done through eDIVE Project Setup window if setup correctly. The main scene should become available in the scene switch window or you can open it manualy.
This module contains the shaders required to display contour lines on an arbitrary 3D object. It also contains the scripts related working with the terrains as well as the monolithic script responsible for the synchronization of the terrain state and related UI.

When cloning manually, must be cloned into a folder called `VRstevnice` otherwise the metadata will break.
